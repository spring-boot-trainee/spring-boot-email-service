package emailservice.listener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.common.EmailRequest;

import emailservice.service.EmailService;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class EmailListener {

	private final EmailService emailService;

	public EmailListener(EmailService emailService) {
		this.emailService = emailService;
	}

	@KafkaListener(topics = "topic-activation-email")
	public void listenForActivationEmail(EmailRequest request) {
		log.info("Kafka received activation-email: {}", request);
		emailService.send(request.getTo(), request.getSubject(), request.getContent());
	}

	// beginner use String
	@KafkaListener(topics = "topic-example")
	public void listenForExample(String message) {
		log.info("Kafka received message: {}", message);
	}

}
